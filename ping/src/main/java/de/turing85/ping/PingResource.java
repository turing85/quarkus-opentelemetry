package de.turing85.ping;

import io.opentelemetry.api.trace.Span;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.UUID;

@ApplicationScoped
@Path("ping")
@Produces(MediaType.TEXT_PLAIN)
public class PingResource {
  private final Sender sender;

  public PingResource(Sender sender) {
    this.sender = sender;
  }

  @GET
  public String ping() {
    return "ping";
  }

  @GET
  @Path("send")
  public String sendPing() {
    String correlationId = UUID.randomUUID().toString();
    Span.current().setAttribute("Correlation-ID", correlationId);
    sender.send(correlationId);
    return "sent";
  }
}
