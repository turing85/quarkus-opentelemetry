package de.turing85.ping;

import io.opentelemetry.context.Context;
import io.opentelemetry.instrumentation.annotations.SpanAttribute;
import io.smallrye.reactive.messaging.TracingMetadata;
import io.smallrye.reactive.messaging.kafka.api.OutgoingKafkaRecordMetadata;
import org.apache.kafka.common.header.internals.RecordHeader;
import org.eclipse.microprofile.reactive.messaging.Channel;
import org.eclipse.microprofile.reactive.messaging.Emitter;
import org.eclipse.microprofile.reactive.messaging.Message;
import org.eclipse.microprofile.reactive.messaging.Metadata;

import javax.enterprise.context.ApplicationScoped;
import java.nio.charset.StandardCharsets;
import java.util.List;

@ApplicationScoped
public class Sender {
  @Channel("hello-channel")
  Emitter<String> helloChannel;

  void send(@SpanAttribute("Correlation-ID") String correlationId) {
    helloChannel.send(Message.of(
        "hello",
        Metadata.of(
          TracingMetadata.withCurrent(Context.current()),
          OutgoingKafkaRecordMetadata.builder()
              .withHeaders(List.of(new RecordHeader("Correlation-ID", correlationId.getBytes(StandardCharsets.UTF_8))))
              .build())));
  }
}
