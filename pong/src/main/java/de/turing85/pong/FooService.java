package de.turing85.pong;

import io.opentelemetry.instrumentation.annotations.SpanAttribute;
import io.opentelemetry.instrumentation.annotations.WithSpan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class FooService {
    private static final Logger LOG = LoggerFactory.getLogger(FooService.class);

    @WithSpan("fooer")
    void foo(String message, @SpanAttribute("Correlation-ID") String correlationId) {
        LOG.info("In fooer, received message \"{}\" with correlationID \"{}\"", message, correlationId);
    }
}
