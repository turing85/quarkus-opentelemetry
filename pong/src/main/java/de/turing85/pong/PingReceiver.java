package de.turing85.pong;

import io.opentelemetry.api.trace.Span;
import io.opentelemetry.context.Scope;
import io.quarkus.opentelemetry.runtime.QuarkusContextStorage;
import io.smallrye.mutiny.Uni;
import io.smallrye.reactive.messaging.TracingMetadata;
import io.smallrye.reactive.messaging.kafka.api.IncomingKafkaRecordMetadata;
import org.apache.kafka.common.header.Header;
import org.eclipse.microprofile.context.ManagedExecutor;
import org.eclipse.microprofile.reactive.messaging.Incoming;
import org.eclipse.microprofile.reactive.messaging.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.util.Iterator;
import java.util.Optional;
import java.util.UUID;

@ApplicationScoped
public class PingReceiver {

  private static final Logger LOG = LoggerFactory.getLogger(PingReceiver.class);

  private final ManagedExecutor executor;
  private final FooService fooService;

  public PingReceiver(@SuppressWarnings("CdiInjectionPointsInspection") ManagedExecutor executor, FooService fooService) {
    this.executor = executor;
    this.fooService = fooService;
  }

  @Incoming("hello-channel")
  public Uni<Void> process(Message<String> message) {
    final String correlationId = message.getMetadata(IncomingKafkaRecordMetadata.class)
        .map(IncomingKafkaRecordMetadata::getHeaders)
        .map(headers -> headers.headers("Correlation-ID"))
        .map(Iterable::iterator)
        .filter(Iterator::hasNext)
        .map(Iterator::next)
        .map(Header::value)
        .map(bytes -> new String(bytes, StandardCharsets.UTF_8))
        .orElseGet(() -> UUID.randomUUID().toString());
    Span.current().setAttribute("Correlation-ID", correlationId);
    return Uni.createFrom().item(message)
        .onItem()
            .invoke(() -> LOG.info("Received message \"{}\"", message.getPayload()))
        .emitOn(executor)
            .onItem()
            .delayIt().by(Duration.ofSeconds(5))
            .map(m -> {
              final Optional<Scope> scope = message.getMetadata(TracingMetadata.class)
                  .map(metadata ->
                      QuarkusContextStorage.INSTANCE.attach(metadata.getCurrentContext()));
              fooService.foo(message.getPayload(), correlationId);
              return scope;
            })
            .onItem()
                .invoke(maybeScope -> maybeScope.ifPresent(Scope::close))
            .onItem()
                .transformToUni(m -> Uni.createFrom().completionStage(message.ack()));
  }
}
